const cluster = [];
const distancias = [];

function stripUndefined(arr) {
    return arr.reduce(function (result, item) {
        result.push(Array.isArray(item) && !item.length ? stripUndefined(item) : item);
        return result;
    }, []);
}

// Función de vectorización
function Vec2(p1, p2, tag) {
    this.p1 = p1;
    this.p2 = p2;
    this.tag = tag;
    this.val = p1 + p2;

    this.euclid = (vec) => {
        let dist = Math.sqrt(Math.pow(this.p1 - vec.p1, 2) + Math.pow(this.p2 - vec.p2, 2));
        return dist;
    }
}

// Función para calcular el centroide en una partición
function centroid(particion) {
    const valores = [];
    
    for (let i = 0; i < particion.length; i++) {
        valores[i] = particion[i].p2;       
    }

    const max = Math.max(...valores);
    const pos = valores.findIndex(e => e === max);

    return particion[pos];
}

// Funcion de asignación para el posterior calculo de espacio vectorial
function asignarCluster(vector) {
    if (vector === undefined) {
        return false;
    }
    
    var punto1 = 0;
    var punto2 = 0;

    for (let i = 0; i < cluster.length; i++) {
        distancias[i] = vector.euclid(cluster[i].centroide);
    }

    const min = Math.min(...distancias);
    const pos = distancias.findIndex(e => e === min);

    if (cluster[pos].arr === undefined ) {
        cluster[pos].arr = [];
    }

    cluster[pos].arr.push(vector)

    for (let j = 0; j < cluster[pos].arr.length; j++) {
        punto1 += cluster[pos].arr[j].p1;
        punto2 += cluster[pos].arr[j].p2;
    }

    punto1 /= cluster[pos].arr.length;
    punto2 /= cluster[pos].arr.length;

    cluster[pos].centroide = new Vec2(punto1, punto2);
}

// Funcion para la primera distribución en la primera iteración
function aplicarObj(particiones) {
    const partiFlat = [].concat.apply([], particiones);
    const horariosDia = {};

    let counter = 0;

    cluster.forEach((c, i) => {
        i++;
        horariosDia["horario " + i] = [];

        for (let index = 0; index < c.arr.length; index++) {
            if (partiFlat[counter] === undefined) { continue; }

            horariosDia["horario " + i].push(partiFlat[counter]);
        
            counter++;
        }
    });

    return horariosDia;
}

function aplicarArr(particiones) {
    const partiFlat = [].concat.apply([], particiones);
    const horariosDia = [];

    let counter = 0;

    cluster.forEach((c, i) => {
        horariosDia[i] = [];

        for (let index = 0; index < c.arr.length; index++) {
            if (partiFlat[counter] === undefined) { continue; }

            horariosDia[i].push(partiFlat[counter]);
        
            counter++;
        }
    });

    return horariosDia;
}

// Funcion de Clustering (un conjunto de particiones)
function clustering(...particiones) {

    for (let i = 0; i < particiones.length; i++) {
        if (cluster[i] === undefined) {
            cluster[i] = { centroide: centroid(particiones[i]) };
        }
    }

    particiones.forEach(p => {
        for (let j = 0; j < p.length; j++) {
            asignarCluster(p[j], cluster);
        }        
    });

    const horarios = aplicarArr(particiones, cluster);

    return horarios;
}



let particion1 = [
    new Vec2(1, 167, "9:00 - 9:30"),
    new Vec2(2, 173, "9:30 - 10:00"),
    new Vec2(3, 179, "10:00 - 10:30"), 
    new Vec2(4, 172, "10:30 - 11:00"),
    new Vec2(5, 147, "11:00 - 11:30"),   
];

let particion2 = [
    new Vec2(6, 156, "11:30 - 12:00"),
    new Vec2(7, 138, "12:00 - 12:30"), 
    new Vec2(8, 140, "12:30 - 13:00"),
    new Vec2(9, 114, "13:00 - 13:30"),
    new Vec2(10, 117, "13:30 - 14:00")
];

let particion3 = [
    new Vec2(11, 110, "14:00 - 14:30"),
    new Vec2(12, 110, "14:30 - 15:00"),
    new Vec2(13, 95, "15:00 - 15:30"), 
    new Vec2(14, 104, "15:30 - 16:00"),
    new Vec2(15, 105, "16:00 - 16:30"),
];

let particion4 = [
    new Vec2(16, 98, "16:30 - 17:00"), 
    new Vec2(17, 101, "17:00 - 17:30"),
    new Vec2(18, 101, "17:30 - 18:00"),
    new Vec2(19, 87, "18:00 - 18:30"), 
    new Vec2(20, 91, "18:30 - 19:00"), 
];

let particion5 = [
    new Vec2(21, 48, "19:00 - 19:30"),
    new Vec2(22, 64, "19:30 - 20:00"),
    new Vec2(23, 5, "20:00 - 20:30"),
    new Vec2(24, 7, "20:30 - 21:00"),
    new Vec2(25, 2, "21:00 - 21:30"),
    new Vec2(26, 2, "21:30 - 22:00"),
];

const iter1 = clustering(particion1, particion2, particion3, particion4, particion5);

console.log(iter1)

